<?php namespace App\Libraries\Helpers;


class Gravatar {

    public static function src($email, $size = 200)
    {
        echo Gravatar::getSrc($email, $size);
    }

    public static function image($email, $size = 200, $class = '')
    {
        echo "<img src='" . Gravatar::getSrc($email, $size) . "' width='$size' height='$size' class='$class'>";
    }

    private static function getSrc($email, $size)
    {
        return "http://www.gravatar.com/avatar/" . md5($email) . "?s=" . $size;
    }

}
