<?php

namespace App;


class Comment extends \Lanz\Commentable\Comment
{

    function user()
    {
        return User::findOrNew($this->user_id);
    }
}
