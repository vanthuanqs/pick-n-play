<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lanz\Commentable\Commentable;

class Game extends Model {

    use SoftDeletes;
    use Commentable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'games';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'field_id', 'cost', 'start_at', 'game_length', 'published_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = ['start_at', 'published_at', 'deleted_at'];

    //protected $dateFormat = 'Y-m-d\TH:i';

    public function field()
    {
        return $this->belongsTo('\App\Field');
    }

    public function comments()
    {
        return $this->hasMany('\App\Comment');
    }

    public function status()
    {
        if(Carbon::now()->lt($this->start_at))
        {
            return $this->start_at->diffForHumans(Carbon::now());
        }
        else if (Carbon::now()->lt($this->start_at->addMinutes($this->game_length)))
        {
            return 'playing';
        }
        else
        {
            return 'ended';
        }
    }

    public function buyer()
    {
        return User::findOrNew($this->buyer_id);
    }

    public function canBuy()
    {
        $canBuy = true;

        if ($this->buyer_id > 0 || Carbon::now()->gte($this->start_at))
            $canBuy = false;

        return $canBuy;
    }
}
