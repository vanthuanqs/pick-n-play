<?php

namespace App\Http\Controllers;

use App\Game;

use App\Http\Requests;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'publicProfile']);
        $this->user = Auth::user();
    }

    public function myGames()
    {
        $incomingGames = Game::where('buyer_id', '=', $this->user->id)->where('start_at', '>=', Carbon::now())->get();
        $pastGames = Game::where('buyer_id', '=', $this->user->id)->where('start_at', '<', Carbon::now())->get();

        return view('user.games', ['incomingGames' => $incomingGames, 'pastGames' => $pastGames]);
    }

    public function publicProfile($id)
    {
        $user = User::findOrFail($id);

        return view('profile', ['user' => $user]);
    }

    public function getProfile()
    {
        return view('user.profile', ['user' => $this->user]);
    }

    public function postProfile()
    {
        $input = request()->all();
        if($input['is_male'] == '') $input['is_male'] = null;

        $user = Auth::user();

        $user->update($input);

        return redirect(route('user.profile'))->with(['messages' =>['Saved!']]);
    }

    public function changePassword(Request $request)
    {

        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);

        if(Hash::check($request['old_password'], $this->user->password))
        {
            $this->user->password = bcrypt($request['password']);
            $this->user->save();

            return redirect()->back()->with(['messages' => ['Password Changed!']]);
        }
        else
        {
            return redirect()->back()->with(['errors' => ['Incorrect Old Password!']]);
        }
    }
}
