<?php

namespace App\Http\Controllers;

use App\Game;
use App\Transaction;
use Carbon\Carbon;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class GameController extends Controller {

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['postGame', 'myGames']]);
    }

    public function getGame($id)
    {
        $game = Game::findOrFail($id);
        return view('game', ['game' => $game]);
    }

    public function postGame($id, Request $request)
    {
        if (Auth::guest())
        {
            session()->flash('redirect', URL::full());

            return redirect(route('login'));
        }

        $game = Game::findOrFail($id);
        $user = Auth::user();
        $token = $request->input('stripeToken');

        try
        {
            $stripe = Stripe::make();

            $charge = $stripe->charges()->create([
                "amount"      => $game->cost / 100,
                "currency"    => "vnd",
                "source"      => $token,
                "description" => "Charge: #{$game->id} - {$game->title}\nUser: #{$user->id} - {$user->name}"
            ]);

        }
        catch (Exception $err)
        {
            $errors[] = "Some thing gone wrong!";

            return redirect(route('game', $id))->with('errors', $errors);
        }

        if ($charge['status'] == 'succeeded')
        {
            $game->buyer_id = $user->id;
            $game->save();

            $transaction = new Transaction();

            $transaction->amount = $game->cost;
            $transaction->detail = "User #{$user->id} pay game #{$game->id}";

            $transaction->save();

            $messages[] = "Successful payment!";
            $messages[] = "Invite your friends and come to {$game->field->name} on {$game->start_at->format('D, d/m/Y @ h:i a')}";

            return redirect(route('game', $id))->with('messages', $messages);
        }
        else
        {
            $errors[] = "Payment error!";

            return redirect(route('game', $id))->with('errors', $errors);
        }

    }

    public function loadGames($page, $limit)
    {
        $nextGames = Game::where('start_at', '>=', Carbon::now())->where('buyer_id', '==', 0)->orderby('start_at')->skip(($page - 1) * $limit)->take($limit)->get();

        $gameCount = Game::where('start_at', '>=', Carbon::now())->orderby('start_at')->count();

        if ($page + 1 <= ceil($gameCount / $limit))
        {
            $url = route('load-games', [$page + 1, $limit]);
        }
        else
        {
            $url = '';
        }

        return response()->json([
            'url'  => $url,
            'html' => view('layout.game-list', ['games' => $nextGames])->render(),
        ]);
    }
}
