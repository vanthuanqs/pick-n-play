<?php

namespace App\Http\Controllers;

use App\Comment;

use App\Game;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class CommentController extends Controller {

    /**
     * CommentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function delete($id)
    {
        $comment = Comment::findOrFail($id);
        $user = Auth::user();

        if ($user->is_admin || $comment->user_id == $user->id)
        {
            $comment->delete();

            return 'success';
        }
        else
        {
            if (request()->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect()->route('login');
            }
        }
    }

    public function postComment($gameId)
    {
        Game::findOrFail($gameId);

        $input = Request::all();

        $comment = new Comment();

        $comment->user_id = Auth::id();
        $comment->game_id = $gameId;
        $comment->content = $input['content'];

        $comment->save();

        return 'success';
    }

    public function edit($id)
    {
        $input = Request::all();

        $comment = Comment::findOrFail($id);
        $comment->content = $input['content'];
        $comment->save();

        return 'success';
    }
}
