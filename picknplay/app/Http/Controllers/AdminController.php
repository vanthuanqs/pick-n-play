<?php

namespace App\Http\Controllers;

use App\Game;
use Carbon\Carbon;
use Request;

class AdminController extends Controller {

    public function index()
    {
        $games = Game::where('start_at', '>=', Carbon::now())->orderby('start_at')->get();

        return view('admin.index', ['games' => $games]);
    }

    public function addGame()
    {
        $game = new Game;

        $game->start_at = Carbon::now()->addDay();
        $game->start_at->hour = 17;
        $game->start_at->minute = 00;

        $game->published_at = Carbon::now();

        return view('admin.game.add', ['game' => $game]);
    }

    public function editGame($id)
    {
        $game = Game::findorFail($id);

        return view('admin.game.edit', ['game' => $game]);
    }

    public function saveGame()
    {
        $input = Request::all();

        $input['start_at'] = Carbon::createFromFormat('Y-m-d\TH:i', $input['start_at']);
        $input['published_at'] = Carbon::createFromFormat('Y-m-d\TH:i', $input['published_at']);

        if ($input['id'] > 0)
        {
            $game = Game::findOrFail($input['id']);
            $game->update($input);

            return redirect(route('admin.index'))->with('messages', ['Game Updated!']);
        }
        else
        {
            Game::create($input);

            return redirect(route('admin.index'))->with('messages', ['Game Added!']);
        }
    }

    public function deleteGame($id)
    {
        $game = Game::findorFail($id);
        $game->delete();

        return redirect(route('admin.index'))->with('messages', ["Game #{$id} deleted!"]);
    }

    /*
     * Fields
     */
    public function listFields()
    {

    }
}
