<?php

namespace App\Http\Controllers;

use App\User;

use App\Http\Requests;

class ManageController extends Controller
{

    /**
     * ManageController constructor.
     */
    public function __construct()
    {
        $this->middleware('manage');
    }

    public function index()
    {
        $users = User::all();
        return view('manage.index', ['users' => $users]);
    }

    public function setAdmin($id)
    {
        $user = User::findOrFail($id);

        $user->is_admin = true;
        $user->save();

        return redirect()->back()->with(['messages' => ["Saved! $user->name have been set as admin!"]]);

    }

    public function removeAdmin($id)
    {
        $user = User::findOrFail($id);

        if($user->is_manager)
        {
            return redirect()->back()->with(['errors' => ["Can't remove this user from admin!"]]);
        }

        $user->is_admin = false;
        $user->save();

        return redirect()->back()->with(['messages' => ["Saved! $user->name have been remove from admin!"]]);

    }

    public function deleteUser($id)
    {
        $user = User::findOrFail($id);

        if($user->is_admin)
        {
            return redirect()->back()->with(['errors' => ["Can't remove admin! Remove this user from admin first!"]]);
        }

        if($user->is_manager)
        {
            return redirect()->back()->with(['errors' => ["Can't remove this user!"]]);
        }

        $user->delete();

        return redirect()->back()->with(['messages' => ["$user->name have been removed!"]]);

    }
}
