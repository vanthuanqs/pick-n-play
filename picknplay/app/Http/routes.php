<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
 * Frontend area
 */
Route::get('', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('load-games/{page}/{limit}', ['as' => 'load-games', 'uses' => 'GameController@loadGames']);

Route::get('game/{id}', ['as' => 'game', 'uses' => 'GameController@getGame']);
Route::post('game/{id}', ['as' => 'game.buy', 'uses' => 'GameController@postGame']);
Route::post('game/{id}/comment', ['as' => 'game.comment', 'uses' => 'CommentController@postComment', 'middleware' => 'auth']);

Route::get('user/{id}', ['as' => 'user', 'uses' => 'UserController@publicProfile']);

/*
 * User Logged in
 */
Route::group([
    'middleware' => 'auth',
], function()
{
    Route::get('my-games', ['as' => 'user.games', 'uses' => 'UserController@myGames']);
    Route::get('profile', ['as' => 'user.profile', 'uses' => 'UserController@getProfile']);
    Route::post('profile', ['as' => 'user.profile.save', 'uses' => 'UserController@postProfile']);
    Route::post('change-pass', ['as' => 'user.profile.change_pass', 'uses' => 'UserController@changePassword']);

    /*
     * comments
     */
    Route::get('comment/{id}/delete', ['as' => 'comment.delete', 'uses' => 'CommentController@delete']);
    Route::post('comment/{id}/edit', ['as' => 'comment.edit', 'uses' => 'CommentController@edit']);
});



/*
 * Login/out, Register
 */
Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::get('register', ['as' => 'register', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('register', ['uses' => 'Auth\AuthController@postRegister']);

/*
 * Password
 */
Route::get('forgot-password', ['as' => 'forgot-password', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('forgot-password', ['as' => 'forgot-password', 'uses' => 'Auth\PasswordController@postEmail']);

Route::get('reset-password/{token?}', ['as' => 'reset-password', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('reset-password', ['uses' => 'Auth\PasswordController@postReset']);

/*
 * Admin Area
 */
Route::group([
    'prefix'     => 'admin',
    'middleware' => 'admin'
], function ()
{
    Route::get('/', ['as' => 'admin.index', 'uses' => 'AdminController@index']);

    Route::group([
        'prefix' => 'game',
    ], function ()
    {
        Route::get('add', ['as' => 'admin.game.add', 'uses' => 'AdminController@addGame']);
        Route::get('{id}/edit', ['as' => 'admin.game.edit', 'uses' => 'AdminController@editGame']);
        Route::get('{id}/delete', ['as' => 'admin.game.delete', 'uses' => 'AdminController@deleteGame']);
        Route::post('save', ['as' => 'admin.game.save', 'uses' => 'AdminController@saveGame']);
    });

    /*
     * Manage Fields
     */
    /*Route::group([
        'prefix' => 'field',
    ], function ()
    {
        Route::get('/', ['as' => 'admin.field.index', 'uses' => 'AdminController@listFields']);
        Route::get('{id}/edit', ['as' => 'admin.field.edit', 'uses' => 'AdminController@editGame']);
        Route::get('{id}/delete', ['as' => 'admin.field.delete', 'uses' => 'AdminController@deleteGame']);
        Route::post('save', ['as' => 'admin.field.save', 'uses' => 'AdminController@saveGame']);


    });*/
});

/*
 * SuperAdmin area
 */
Route::group([
    'prefix'     => 'manage',
    'middleware' => 'manage'
], function ()
{
    Route::get('/', ['as' => 'manage.index', 'uses' => 'ManageController@index']);
    Route::get('user/{id}/delete', ['as' => 'manage.user.delete', 'uses' => 'ManageController@deleteUser']);
    Route::get('user/{id}/set-admin', ['as' => 'manage.user.set-admin', 'uses' => 'ManageController@setAdmin']);
    Route::get('user/{id}/remove-admin', ['as' => 'manage.user.remove-admin', 'uses' => 'ManageController@removeAdmin']);

});

