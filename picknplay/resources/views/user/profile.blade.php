@extends('app')

@section('title', "Edit Profile")

@section('content')

  <div id="edit-profile">
    <a href="https://en.gravatar.com/emails/" target="_blank" class="avatar">
      {{ Gravatar::image($user->email, 200) }}
    </a>

    {!! Form::model($user, ['route' => 'user.profile.save']) !!}

      {!! Form::label('name', 'Name:', ['class' => 'label']) !!}
      {!! Form::text('name', null, ['class' => 'input', 'placeholder' => 'Name']) !!}

      {!! Form::label('email', 'Email:', ['class' => 'label']) !!}
      {!! Form::email('email', null, ['class' => 'input', 'placeholder' => 'Email']) !!}

      {!! Form::label('phone', 'Phone number:', ['class' => 'label']) !!}
      {!! Form::text('phone', null, ['class' => 'input', 'placeholder' => 'Phone Number']) !!}

      {!! Form::label('birthday', 'Birthday:', ['class' => 'label']) !!}
      {!! Form::input('date', 'birthday', $user->birthday->format('Y-m-d'), ['class' => 'input', 'placeholder' => 'Birthday']) !!}

      {!! Form::label('is_male', 'Gender:', ['class' => 'label']) !!}
      {!! Form::select('is_male', [ '' => '', '1' => 'Male', '0' => 'Female'], null, ['class' => 'select']) !!}

      {!! Form::label('address', 'Address:', ['class' => 'label']) !!}
      {!! Form::text('address', null, ['class' => 'input', 'placeholder' => 'Address']) !!}

      {!! Form::submit('Save', array('class' => 'button')) !!}

    {!! Form::close() !!}

    <h3>Change your password?</h3>
    {!! Form::open(['route' => ['user.profile.change_pass']]) !!}

      {!! Form::password('old_password', ['class' => 'input', 'placeholder'=>'Current Password', 'required'=>'required']) !!} <br> <br>

      {!! Form::password('password', ['class' => 'input', 'placeholder'=>'New Password', 'required'=>'required']) !!} <br> <br>
      {!! Form::password('password_confirmation', ['class' => 'input', 'placeholder'=>'New Password Confirmation', 'required'=>'required']) !!}

      {!! Form::submit('Change Password', ['class' => 'button']) !!}
    {!! Form::close() !!}
  </div>

@endsection
