@extends('app')

@section('title', 'My Game List')

@section('content')


  <div id="user-games">

    @if(count($incomingGames) > 0)
      <div id="incoming-games" class="game-list-container">

        <h2 class="title"><span>Incoming</span> games</h2>
        <p class="subtitle"><span>Don't</span> forget to join these game on time!</p>

        <div class="game-list">
          @include('layout.game-list', ['games' => $incomingGames])
        </div>

      </div>
    @endif

    @if(count($pastGames) > 0)
      <div id="past-games" class="game-list-container">

        <h2 class="title"><span>Past</span> games</h2>

        <div class="game-list">
          @include('layout.game-list', ['games' => $pastGames])
        </div>

      </div>
    @endif

    @if(count($incomingGames) + count($pastGames) == 0)
      <h2 class="title">No <span>game</span> found!</h2>
      <p class="subtitle">Haven't join any game? Check some incoming games <a href="{{ route('home') }}" class="black-link">here</a>!</p>
    @endif

  </div>

  </div>

@endsection
