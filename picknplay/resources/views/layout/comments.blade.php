@if(sizeof($comments) > 0)
  <h2 class="title">Comments</h2>

  <div class="comments">
    @foreach($comments as $comment)
      <div class="comment">

        <div class="avatar">
          {!! Gravatar::image($comment->user()->email, 70) !!}
        </div>

        <div class="content">

          <div class="user">
            <a href="{{ route('user', $comment->user()->id)  }}" class="black-link" target="_blank">
              <b>{{ $comment->user()->name }}</b>
            </a>
            ·
            <span title="{{ $comment->created_at->format('D, d/m/Y @ h:i a') }}">
              {{ $comment->created_at->diffForHumans(\Carbon\Carbon::now()) }}
            </span>
          </div>

          <p>{!! nl2br(e($comment->content)) !!}</p>

          <div class="actions">

            @if(Auth::user())

              @if(Auth::user()->id == $comment->user_id)
                <a href="{{ route('comment.edit', $comment->id) }}" data-commentid="{{ $comment->id }}" class="black-link">Edit</a>
              @endif

              @if(Auth::user()->is_admin || Auth::user()->id == $comment->user_id)
                <a href="{{ route('comment.delete', $comment->id) }}" data-commentid="{{ $comment->id }}" class="black-link">Delete</a>
              @endif

            @endif

          </div>
        </div>
      </div>
    @endforeach

  </div>

@endif