<form action="{{ route('game.comment', $game->id) }}" method="post">

  <input type="hidden" name="_token" value="{{ csrf_token() }}">

  <h2>New Comment</h2>
  <textarea name="comment" id="new-comment" class="textarea" placeholder="Press Enter to Submit"></textarea>

</form>