@if (isset($errors) && count($errors) > 0)

  <div class="alert errors">
    <ul>

      @foreach ($errors as $error)
        <li>{{ $error }}</li>
      @endforeach


      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach

    </ul>
  </div>

@endif


@if (Session::has('errors'))
  <div class="alert messages">
    <ul>

      @foreach (session('errors') as $message)
        <li>{{ $message }}</li>
      @endforeach

    </ul>
  </div>
@endif


@if (Session::has('messages'))
  <div class="alert messages">
    <ul>

      @foreach (session('messages') as $message)
        <li>{{ $message }}</li>
      @endforeach

    </ul>
  </div>
@endif