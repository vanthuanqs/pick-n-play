@if(isset($games))

  @foreach($games as $game)

    <a href="{{ route('game', $game->id) }}">
      <div class="game">

        <div class="title-block">

          <div class="g-sport">
            <i class="fa fa-futbol-o"></i>
          </div>

          <div class="g-title">{{ $game->title }}</div>
        </div>

        <div class="detail">

          <div class="g-cost">
            <i class="fa fa-money"></i>
            {{ $game->cost }} VND
          </div>

          <div class="g-date">
            <i class="fa fa-clock-o"></i> {{ $game->start_at->format('D, d/m/Y @ h:i a') }}
          </div>

          <div class="g-location">
            <i class="fa fa-map-marker"></i> {{ $game->field->name }}
          </div>

        </div>

      </div>
    </a>

  @endforeach

@endif