@extends('app')

@section('title', 'Reset Password')

@section('content')

	<div id="reset-pass">

		<h1>Reset Passsword!</h1>
		{!! Form::open(['route' => 'reset-password']) !!}

			{!! Form::hidden('token', $token) !!}

			{!! Form::label('email', 'Enter your email and new password') !!} <br>

			{!! Form::text('email', old('email'), ['id' => 'email', 'class' =>'input', 'placeholder' => 'Your email']) !!} <br>

			{!! Form::password('password', ['class' =>'input', 'placeholder' => 'Password']) !!} <br>

			{!! Form::password('password_confirmation', ['class' =>'input', 'placeholder' => 'Confirm Password']) !!} <br>

			{!! Form::submit('Save', ['id' => 'email', 'class' =>'button']) !!}

		{!! Form::close() !!}

	</div>

@endsection
