@extends('app')


@section('content')

  @section('title', 'Login or Register')

  <div id="join-login">

    <div class="register">

      <h1 class="page-title">Register Now!</h1>

      {!! Form::open(['route' => 'register']) !!}

        {!! Form::text('name', old('name'), ['class' => 'input', 'placeholder' => 'Full Name']) !!} <br>

        {!! Form::text('email', old('name'), ['class' => 'input', 'placeholder' => 'Email']) !!} <br>

        {!! Form::password('password', ['class' => 'input', 'placeholder' => 'Password']) !!} <br>

        {!! Form::password('password_confirmation', ['class' => 'input', 'placeholder' => 'Confirm Password']) !!} <br>

        <p class="terms">By clicking join you agree <br>to the terms of use and privacy policy</p>

        {!! Form::submit('Register', ['class' => 'button']) !!}

      {!! Form::close() !!}

    </div>

    <div class="login-container">
      <div class="login">
        <h1>Login</h1>

        {!! Form::open(['route' => 'login']) !!}

          {!! Form::text('email', old('name'), ['class' => 'input', 'placeholder' => 'Email']) !!} <br>

          {!! Form::password('password', ['class' => 'input', 'placeholder' => 'Password']) !!} <br>

          {!! Form::checkbox('remember', '1' , true, ['class' => 'checkbox', 'id' => 'remember']) !!}
          {!! Form::label('remember', ' Remember Me') !!}  <br>

          {!! Form::submit('Login', ['class' => 'button']) !!}

        {!! Form::close() !!}

        <div class="forgot-pass">
          <a href="{{ route('forgot-password') }}">Forgot your password?</a>
        </div>

      </div>
    </div>

  </div>

@endsection
