@extends('app')

@section('title', 'Forgot Password')

@section('content')

  @if (session('status'))
    <div class="alert success">
      <ul>
        <li>
          {{ session('status') }}
        </li>
      </ul>
    </div>
  @endif

  <div id="forgot-pass">
    <h1>Forgot your Password?</h1>

    {!! Form::open() !!}

      {!! Form::label('email', 'Enter your email address') !!} <br>

      {!! Form::text('email', old('email'), ['id' => 'email', 'class' =>'input', 'placeholder' => 'Your email']) !!}

      {!! Form::submit('Send reset password', ['id' => 'email', 'class' =>'button']) !!}

    {!! Form::close() !!}

  </div>

@endsection
