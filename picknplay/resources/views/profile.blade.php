@extends('app')

@section('title', "$user->name - Profile")

@section('content')

  <div id="public-profile">
    {{ Gravatar::image($user->email, 200, 'avatar') }}

    <ul id="user-detail">

      <h2>{{ $user->name }}</h2>

      <li>
        <span>Join in:</span>{{ $user->created_at->format('D, d/m/Y') }}
      </li>

      <li>
        <span>Email:</span><a href="mailto:{{ $user->email }}" class="black-link">{{ $user->email }}</a>
      </li>

      <li>
        <span>Games Played:</span> {{ \App\Game::where('buyer_id', '=', $user->id)->count() }}
      </li>

    </ul>

  </div>

@endsection
