@extends('app')

@section('title', 'Add new Game')

@section('content')

  <div id="add-game">

    <a href="{{ route('admin.index') }}" class="black-link back"><i class="fa fa-long-arrow-left"></i> Back</a>

    <center>
      <h1>Add new Game</h1>
    </center>

    @include('admin.game.form')

  </div>

@endsection
