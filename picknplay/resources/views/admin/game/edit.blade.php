@extends('app')

@section('title', 'Add new Game')



@section('content')

  <div id="add-game">

    <a href="{{ route('admin.index') }}" class="black-link back"><i class="fa fa-long-arrow-left"></i> Back</a>

    <a href="{!! route('admin.game.delete', $game->id) !!}" class="delete-game black-link">Delete Game <i class="fa fa-trash-o"></i></a>

    <center>
      <h1>Edit Game #{{ $game->id }}</h1>
    </center>

    @include('admin.game.form')


  </div>

@endsection
