{!! Form::model($game, ['route' => 'admin.game.save', 'class' => 'game-form']) !!}

{!! Form::hidden('id') !!}

{{-- field id --}}

{!! Form::label('title', 'Game Title:', ['class' => 'label']) !!}
{!! Form::text('title', null, ['class' => 'input', 'placeholder' => 'Game Title']) !!}

{!! Form::label('description', 'Some description about this game:', ['class' => 'label']) !!}
{!! Form::textarea('description', null, ['class' => 'textarea']) !!}

{!! Form::label('field_id', 'Field:', ['class' => 'label']) !!}
{!! Form::select('field_id', \App\Field::lists('name', 'id'), null, ['class' => 'select']) !!}

{!! Form::label('start_at', 'Game Start Time:', ['class' => 'label']) !!}
{!! Form::input('datetime-local', 'start_at', $game->start_at->format('Y-m-d\TH:i'), ['class' => 'input',]) !!}

{!! Form::label('game_length', 'Game length in minutes:', ['class' => 'label']) !!}
{!! Form::input('number', 'game_length', 60, ['class' => 'input', 'placeholder' => 'Game Length', 'step' => 5]) !!}

{!! Form::label('published_at', 'Published Time:', ['class' => 'label']) !!}
{!! Form::input('datetime-local', 'published_at', $game->published_at->format('Y-m-d\TH:i'), [ 'class' => 'input'])!!}

{!! Form::label('cost', 'Game Cost (in VND):', ['class' => 'label']) !!}
{!! Form::input('number', 'cost', 200000, ['class' => 'input', 'placeholder' => 'Game Length', 'step' => 10000]) !!}

{!! Form::submit('Save game', ['class' => 'button']) !!}

{!! Form::close() !!}