@extends('app')

@section('title', 'Admin')

@section('css')
  @parent
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
@endsection

@section('content')

  <div id="admin-index">

    <div class="add-game-link">
      <h1>Add new Game</h1>
      <p>Click button bellow to add new game:</p>
      {!! Html::link(route('admin.game.add'), 'Add new game', ['class' => 'button']) !!}
    </div>

    <h2 class="title">Game List</h2>

    <div id="game-list-container">

      <table id="game-list" cellspacing="0" width="100%">

        <thead>
        <tr>
          <th>#ID</th>
          <th>Title</th>
          <th>Start time</th>
          <th>Game Length</th>
          <th>Cost</th>
          <th>Buyer</th>
          <th>Field</th>
          <th>Edit</th>
        </tr>
        </thead>

        <tbody>
        @foreach($games as $game)
          <tr>
            <th class="game_id">{{ $game->id }}</th>
            <th class="game_title"><a href="{!! route('game', $game->id) !!}"class="black-link" target="_blank">{{ $game->title }}</a></th>
            <th class="game_time">{{ $game->start_at }} <i>({{ $game->status() }})</i></th>
            <th class="game_length">{{ $game->game_length }}</th>
            <th class="game_cost">{{ $game->cost }}</th>
            <th class="game_cost">{{ $game->buyer()->name }}</th>
            <th class="game_field">{{ $game->field->name }}</th>
            <th class="game_edit">
              @if(\Carbon\Carbon::now()->lt($game->start_at))
                {!! Html::link(route('admin.game.edit', $game->id), '', ['class' => 'black-link fa fa-pencil']) !!}
              @endif
            </th>
          </tr>
        @endforeach
        </tbody>

        <tfoot>
        <tr>
          <th>#ID</th>
          <th>Title</th>
          <th>Start time</th>
          <th>Game Length</th>
          <th>Cost</th>
          <th>Buyer</th>
          <th>Field</th>
          <th>Edit</th>
        </tr>
        </tfoot>

      </table>

    </div>

    {{--<h1>Mange Fields</h1>

    <div class="add-game-link">
      <p>Add, edit or delete any field:</p>
      {!! Html::link(route('admin.field.index'), 'Manage Fields', ['class' => 'button']) !!}
    </div>--}}

  </div>

@endsection

@section('js')
  @parent

  <script src="{!! URL::asset('js/app.js') !!}"></script>
  <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

  <script>
    $(function () {
      $('#game-list').DataTable({
        pageLength: 25,
        order: [[ 0, 'desc' ]]
        //order: [[ 2, 'asc' ]]
      });
    })
  </script>
@endsection