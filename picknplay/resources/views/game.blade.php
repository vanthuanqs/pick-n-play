@extends('app')

@section('title', $game->title)

@section('content')

  <div id="game-page">

    <ul id="game-detail">

      <h2 class="title">{{ $game->title }}</h2>

      <li>
        <span>Description:</span>

        <p>{!! nl2br(e($game->description)) !!}</p>
      </li>

      <li><span>Start Time:</span> {{$game->start_at->format('D, d/m/Y @ h:i a') }}. <i><b>({{ $game->status() }})</b></i></li>

      <li><span>Game Length:</span> {{ $game->game_length }} minutes</li>

      <li><span>Location:</span> {{ $game->field->name }}</li>

      <li><span>Cost:</span> {{ $game->cost }} VND</li>

      <li><span>Game link:</span> <a href="{{ route('game', $game->id) }}" class="black-link">{{ route('game', $game->id) }}</a></li>

      @if($game->buyer_id > 0)
        <li><span>Buyer:</span> #{{$game->buyer()->id}} - {{$game->buyer()->name}}</li>
      @endif
    </ul>

    @if($game->canBuy())

      @if (Auth::guest())
        <a href="{{ route('login') }}" class="button">LOGIN TO BUY</a>
      @else
        <form id="stripe-form" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <script
              src="https://checkout.stripe.com/checkout.js" class="stripe-button"
              data-key="{{ env('STRIPE_PK') }}"
              data-amount="{{ $game->cost }}"
              data-currency="VND"
              data-name="Pick 'n Play"
              data-description="Buy game #{{ $game->id }}: {{ $game->title }}"
              data-image="{{ asset('/images/logo-square.png') }}"
              data-locale="auto">
          </script>

        </form>
      @endif

    @endif

    <div id="comment-container">
      @include('layout.comments', ['comments' => $game->comments])

      @if(!Auth::guest())
        @include('layout.comment-input')
      @endif
    </div>

  </div>

@endsection
