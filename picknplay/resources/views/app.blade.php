<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="classification" content="Organized Pickup Sports">
  <title>@yield('title', 'Soccer Field Booking System') - Pick 'n Play</title>

  @section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    @show

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,900&subset=vietnamese,latin' rel='stylesheet' type='text/css'>
</head>

<body>

<header>
  <a id="logo" href="{{ route('home') }}">
    <h1>Pick 'n <span>Play</span></h1>
  </a>

  {{--<div id="search-bar">
    <input id="search" type="text" placeholder="Search Sports, Facilities & Players">

    <div id="search_result" title="Search Result"></div>
  </div>--}}

  <ul class="menu-right" id="menu-right">

    @if (Auth::guest())

      <li>
        <div><a href="{{ route('login') }}">
            <i class="fa fa-sign-in"></i> Login
          </a></div>
      </li>

      <li>
        <div><a href="{{ route('register') }}">
            <i class="fa fa-user-plus"></i> Register
          </a></div>
      </li>

    @else

      @if(Auth::user()->is_manager)
        <li>
          <div><a href="{{ route('manage.index') }}">Manage Users</a></div>
        </li>
      @endif

      @if(Auth::user()->is_admin)
        <li>
          <div><a href="{{ route('admin.index') }}">Manage Games</a></div>
        </li>
      @endif

      <li>
        <div class="navbar-pic">
          <a href="{{ route('home') }}" alt="Your Hub">{{ \Gravatar::image(Auth::user()->email, 100) }}</a>
        </div>
      </li>

      <li class="user-menu">
        <div>{{ Auth::user()->name }}&nbsp;<i class="fa fa-angle-down"></i></div>

        <ul class="expanded-menu">
          <li>
            <a href="{{ route('home') }}"><i class="fa fa-home"></i>Home</a>
          </li>

          <li>
            <a href="{{ route('user.games') }}"><i class="fa fa-futbol-o"></i>My games</a>
          </li>

          <li>
            <a href="{{ route('user.profile') }}"><i class="fa fa-user"></i>Profile</a></li>
          </li>

          <li>
            <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i>Logout</a>
          </li>
        </ul>

      </li>
    @endif
  </ul>
</header>


<div id="wrapper">
  @section('wrapper')

    <div id="content">

      @include('layout.alerts')

      @yield('content')

    </div>

  @show
</div>


<footer>
  <ul class="flinks" style="margin-right: 30px;text-align:left;">
    <li><a class="hint--top" data-hint="Twitter" target="_blank" href="http://twitter.com/vanthuanqs">
        <i class="fa fa-twitter"></i>
      </a></li>
    <li><a class="hint--top" data-hint="Facebook" target="_blank" href="https://facebook.com/vanthuanqs">
        <i class="fa fa-facebook"></i>
      </a></li>
    <li><a class="hint--top" data-hint="Instagram" target="_blank" href="https://instagram.com/vanthuanqs">
        <i class="fa fa-instagram"></i>
      </a></li>
  </ul>

  <ul class="flinks">
    <li><a href="{{ route('home') }}"><i class="fa fa-home"></i>&nbsp;Hub</a></li>
    <li><a href="{{ route('home') }}/faq"><i class="fa fa-question-circle"></i>&nbsp;FAQs</a></li>
    <li><a href="{{ route('home') }}/grow"><i class="fa fa-users"></i>&nbsp;Become a Host</a></li>
    <li><a href="{{ route('home') }}/privacy-policy"><i class="fa fa-lock"></i>&nbsp;Privacy Policy</a></li>
    <li><a href="{{ route('home') }}/terms-of-use"><i class="fa fa-file-text"></i>&nbsp;Terms of Use</a></li>
    <li><a href="{{ route('home') }}/host"><i class="fa fa-globe"></i>&nbsp;Game Hosts</a></li>
  </ul>

  <ul class="flinks">
    <li>Contact:</li>
    <li><a class="hint--top" data-hint="Become a Host?" href="mailto:{{ config('site.emails.sales') }}">
        <i class="fa fa-envelope"></i></a>
    </li>
    <li><a class="hint--top" data-hint="General Inquiries" href="mailto:{{ config('site.emails.info') }}">
        <i class="fa fa-question"></i>
      </a></li>
    <li>&copy; 2015 Pick 'n Play</li>
  </ul>
</footer>

@section('js')
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="{{ asset('/js/app.js') }}"></script>

@show

</body>
</html>
