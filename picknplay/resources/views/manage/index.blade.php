@extends('app')

@section('title', 'Manage Users')

@section('css')
  @parent
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
@endsection

@section('content')

  <div id="manage-index">

    <h2 class="title">Users</h2>

    <div id="user-list-container">

      <table id="user-list" cellspacing="0" width="100%">

        <thead>
        <tr>
          <th>#ID</th>
          <th>Name</th>
          <th>Email</th>
          <th>Birthday</th>
          <th>Gender</th>
          <th>Is Admin</th>
          <th>Join Date</th>
          <th>Delete</th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
          <tr>
            <th class="user_id">{{ $user->id }}</th>
            <th class="user_name"><a href="{!! route('user', $user->id) !!}"class="black-link" target="_blank">{{ $user->name }}</a></th>
            <th class="user_email">{{ $user->email }}</th>
            <th class="user_birthday">{{ $user->birthday->format('d/m/Y') }}</th>
            <th class="user_gender">{{ $user->is_male == null ? '-' : ($user->is_male ? 'Male' : 'Female') }}</th>

            <th class="user_is_admin">
              @if($user->is_admin)
                <a href="{{ route('manage.user.remove-admin', $user->id) }}" title="Remove from admin" class="black-link"><i class="fa fa-check"></i> Admin</a>
              @else
                <a href="{{ route('manage.user.set-admin', $user->id) }}" class="black-link"><i class="fa fa-edit" title="Set as Admin"></i></a>
              @endif
            </th>

            <th class="user_join_date">{{ $user->created_at->format('D, d/m/Y @ h:i a') }}</th>
            <th class="user_edit">
                {!! Html::link(route('manage.user.delete', $user->id), '', ['class' => 'black-link fa fa-user-times']) !!}
            </th>
          </tr>
        @endforeach
        </tbody>

        <tfoot>
        <tr>
          <th>#ID</th>
          <th>Title</th>
          <th>Start time</th>
          <th>Game Length</th>
          <th>Cost</th>
          <th>Buyer</th>
          <th>Field</th>
          <th>Delete</th>
        </tr>
        </tfoot>

      </table>

    </div>

  </div>

@endsection

@section('js')
  @parent

  <script src="{!! URL::asset('js/app.js') !!}"></script>
  <script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

  <script>
    $(function () {
      $('#user-list').DataTable({
        pageLength: 25,
        order: [[ 0, 'asc' ]]
      });
    })
  </script>
@endsection