@extends('app')

@section('content')

  <div id="home">

    @if (Auth::guest())

      <div class="slider">
        <h1>Play Your Favorite Soccer</h1>

        <p>Find a game. Buy your spot. Show up and Play. It's that easy!</p>
        <a href="{{ route('register') }}" class="button">Join with us</a>
      </div>

    @endif

    <div id="next-games" class="game-list-container">

      <h2 class="title">Next occurrence <span>games</span></h2>

      <p class="subtitle">Find a <span>convenient</span> game for you, buy it and enjoy!</p>

      <div class="game-list"></div>

      <a href="#more" data-url="{{ route('load-games', [0, $limit]) }}" class="button more-game">MORE GAMES</a>

    </div>

  </div>

@endsection
