$(document).ready(function () {

    $('.more-game').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        var container = $(this).prev('.game-list');

        jQuery.ajax({
            url: url,
            dataType: 'json',
            success: function (res) {
                container.append(res.html);

                if (res.url == '') {
                    $('.more-game').remove();
                }
                else {
                    $('.more-game').data('url', res.url);
                }

            }
        });
    }).click();


    /*
     *  COMMENT
     */

    $("#new-comment").keypress(function (event) {
        if (event.keyCode == 13 && !event.shiftKey) {
            var $textarea = $(this);
            var $form = $(this).parent('form');

            $.ajax({
                url: $form.attr('action'),
                method: 'POST',
                data: {
                    _token: $form.find('input[name="_token"]').val(),
                    content: $textarea.val()
                },
                beforeSend: function () {
                    $textarea.prop('disabled', true)
                },
                success: function (res) {
                    if (res == 'success') {
                        window.location.reload();
                    }
                    else {
                        alert("Can't post comment! Please try again later!");
                        $textarea.prop('disabled', false);
                    }
                },
                error: function () {
                    alert("Can't post comment! Please try again later!");
                    $textarea.prop('disabled', false);
                }
            });
            return false;
        }
    });

    $('.comment .actions a[href*="edit"]').click(function (e) {
        e.preventDefault();

        var $this = $(this);
        var $textarea = $("#new-comment");
        var $form = $textarea.parent('form');

        $form.attr('action', $this.attr('href'));
        $form.find('h2').html('Edit Comment:');
        $textarea.val($this.parents('.comment').find('.content p').text());

        $("html, body").animate({scrollTop: $form.offset().top}, '500', 'swing');
    });

    $('.comment .actions a[href*="delete"]').click(function (e) {
        var $this = $(this);
        e.preventDefault();

        if (confirm('Delete this message?') == true) {
            $.ajax({
                url: $(this).attr('href'),
                success: function (res) {
                    if (res == 'success') {
                        $this.parents('.comment').remove();
                    }
                    else {
                        alert("Can't delete! Please try again later!");
                    }
                },
                error: function () {
                    alert("Can't delete! Please try again later!");
                }
            });
        }
    });
})