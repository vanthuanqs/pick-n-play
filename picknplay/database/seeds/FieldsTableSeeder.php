<?php

use App\Field;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        Field::truncate();

        for ($i = 'A'; $i < 'D'; $i++)
        {
            for ($j = 1; $j < 5; $j++)
            {
                Field::create([
                    'name'        => "Field {$i}{$j}",
                    'description' => $faker->text(300)
                ]);
            }
        }
    }
}
