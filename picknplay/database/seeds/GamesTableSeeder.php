<?php

use App\Game;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class GamesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        Game::truncate();

        for ($i = 1; $i < 300; $i++)
        {
            Game::create([
                'title'        => 'Game ' . $i,
                'field_id'     => \App\Field::all()->random(1)->id,
                'description'  => $faker->paragraph(),
                'cost'         => $faker->numberBetween(100, 150) * 1000,
                'start_at'     => \Carbon\Carbon::create(2015, 12,
                    $faker->numberBetween(1, 29), $faker->numberBetween(7, 20)),
                'game_length'  => 60,
                'published_at' => \Carbon\Carbon::now(),
            ]);
        }

        for ($i = 1; $i < 300; $i++)
        {
            Game::create([
                'title'        => 'Game ' . $i,
                'field_id'     => \App\Field::all()->random(1)->id,
                'description'  => $faker->paragraph(),
                'cost'         => $faker->numberBetween(100, 150) * 1000,
                'start_at'     => \Carbon\Carbon::create(2016, 1,
                    $faker->numberBetween(1, 29), $faker->numberBetween(7, 20)),
                'game_length'  => 60,
                'published_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
