<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use \App\User as User;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        User::truncate();

        User::create([
            'name'      => 'Thuan User',
            'email'     => 'vanthuanqs@yahoo.com',
            'phone'     => '01687768464',
            'password'  => bcrypt('123456'),
            'birthday'  => Carbon::create(1993, 8, 26),
            'is_male'   => true,
            'address' => 'Pham Nhu Xuong, Danang',
            'is_admin'   => false,
            'is_manager' => false
        ]);

        User::create([
            'name'      => 'Manager',
            'email'     => 'manager@picknplay.dev',
            'phone'     => '01687768464',
            'password'  => bcrypt('123456'),
            'birthday'  => Carbon::create(1993, 8, 26),
            'is_male'   => true,
            'address' => 'Pham Nhu Xuong, Danang',
            'is_admin'   => true,
            'is_manager' => true
        ]);

        User::create([
            'name'      => 'Admin',
            'email'     => 'admin@picknplay.dev',
            'phone'     => '01687768464',
            'password'  => bcrypt('123456'),
            'birthday'  => Carbon::create(1993, 8, 26),
            'is_male'   => true,
            'address' => 'Pham Nhu Xuong, Danang',
            'is_admin'   => true,
            'is_manager' => false
        ]);

        for ($i = 1; $i < 15; $i++)
        {
            User::create([
                'name'     => $faker->name(),
                'email'    => $faker->email(),
                'phone'    => '01687' . $faker->numberBetween(100000, 999999),
                'birthday' => date('Y-m-d'),
                'is_male'  => $faker->boolean(),
                'address' => $faker->address(),
                'password' => bcrypt('123456')
            ]);
        }
    }
}