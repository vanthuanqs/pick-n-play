<?php

use App\Game;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Lanz\Commentable\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $games = Game::all();

        Comment::truncate();

        foreach($games as $game)
        {
            for($i = 0; $i < $faker->numberBetween(2, 5); $i++)
            {
                $comment = new Comment();

                $comment->game_id = $game->id;
                $comment->user_id = $faker->numberBetween(1, \App\User::count());
                $comment->content = $faker->text();

                $comment->save();
            }
        }

    }
}
