<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration {

    private $table = 'games';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('field_id')->unsigned();

            $table->string('title');
            //$table->string('slug');
            $table->string('description');

            $table->dateTime('start_at');
            $table->integer('game_length')->unsigned();     //in minutes
            $table->dateTime('published_at')->default(DB::raw('CURRENT_TIMESTAMP'));


            $table->integer('cost');
            $table->integer('buyer_id')->unsigned();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('field_id')->references('id')->on('fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }

}
