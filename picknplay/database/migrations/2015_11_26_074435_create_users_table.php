<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	private $table = 'users';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create($this->table, function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('name');

			$table->string('email')->unique();
			$table->string('phone');

			$table->date('birthday');
			$table->boolean('is_male')->nullable();	// 1: male, 0: female, null: un-define

			$table->string('address')->nullable();

			$table->string('password', 60);

			$table->boolean('is_admin');
			$table->boolean('is_manager');

			$table->rememberToken();

			//$table->boolean('isBlocked');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop($this->table);
	}

}
