<?php

/*
 * Constants for this site
 */

return [
    'name'   => "Pick 'n Play",
    "home"   => env('APP_URL'),
    "domain" => "picknplay.dev",
    "email"  => "admin@picknplay.dev",
    "emails" => [
        "admin" => "admin@picknplay.dev",
        "sales" => "sale@picknplay.dev",
        "info"  => "info@picknplay.dev"
    ]
];